From f21c8614daeb70a021c128b97c000a92652cf6f8 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Jonas=20=C3=85dahl?= <jadahl@gmail.com>
Date: Thu, 24 Feb 2022 12:32:27 +0100
Subject: [PATCH] monitor-manager: Add NightLightSupported property to
 DisplayConfig

This checks whether it's possible to set a CRTC GAMMA_LUT, which is what
is necessary to implement night light.
---
 src/backends/meta-monitor-manager.c           | 76 ++++++++++++++++---
 .../native/meta-monitor-manager-native.c      | 25 ++++--
 .../x11/meta-monitor-manager-xrandr.c         |  9 ++-
 src/org.gnome.Mutter.DisplayConfig.xml        |  7 ++
 4 files changed, 99 insertions(+), 18 deletions(-)

diff --git a/src/backends/meta-monitor-manager.c b/src/backends/meta-monitor-manager.c
index 75146950c3..0f30b3de25 100644
--- a/src/backends/meta-monitor-manager.c
+++ b/src/backends/meta-monitor-manager.c
@@ -952,6 +952,59 @@ update_panel_orientation_managed (MetaMonitorManager *manager)
     handle_orientation_change (orientation_manager, manager);
 }
 
+static void
+meta_monitor_manager_get_crtc_gamma (MetaMonitorManager  *manager,
+                                     MetaCrtc            *crtc,
+                                     size_t              *size,
+                                     unsigned short     **red,
+                                     unsigned short     **green,
+                                     unsigned short     **blue)
+{
+  MetaMonitorManagerClass *klass = META_MONITOR_MANAGER_GET_CLASS (manager);
+
+  if (klass->get_crtc_gamma)
+    {
+      klass->get_crtc_gamma (manager, crtc, size, red, green, blue);
+    }
+  else
+    {
+      if (size)
+        *size = 0;
+      if (red)
+        *red = NULL;
+      if (green)
+        *green = NULL;
+      if (blue)
+        *blue = NULL;
+    }
+}
+
+static gboolean
+is_night_light_supported (MetaMonitorManager *manager)
+{
+  GList *l;
+
+  for (l = meta_backend_get_gpus (manager->backend); l; l = l->next)
+    {
+      MetaGpu *gpu = l->data;
+      GList *l_crtc;
+
+      for (l_crtc = meta_gpu_get_crtcs (gpu); l_crtc; l_crtc = l_crtc->next)
+        {
+          MetaCrtc *crtc = l_crtc->data;
+          size_t gamma_lut_size;
+
+          meta_monitor_manager_get_crtc_gamma (manager, crtc,
+                                               &gamma_lut_size,
+                                               NULL, NULL, NULL);
+          if (gamma_lut_size > 0)
+            return TRUE;
+        }
+    }
+
+  return FALSE;
+}
+
 void
 meta_monitor_manager_setup (MetaMonitorManager *manager)
 {
@@ -967,7 +1020,6 @@ meta_monitor_manager_setup (MetaMonitorManager *manager)
   meta_dbus_display_config_set_apply_monitors_config_allowed (manager->display_config,
                                                               policy->enable_dbus);
 
-
   meta_monitor_manager_read_current_state (manager);
 
   meta_monitor_manager_ensure_initial_config (manager);
@@ -2445,7 +2497,6 @@ meta_monitor_manager_handle_get_crtc_gamma  (MetaDBusDisplayConfig *skeleton,
                                              guint                  crtc_id,
                                              MetaMonitorManager    *manager)
 {
-  MetaMonitorManagerClass *klass;
   GList *combined_crtcs;
   MetaCrtc *crtc;
   gsize size;
@@ -2476,14 +2527,8 @@ meta_monitor_manager_handle_get_crtc_gamma  (MetaDBusDisplayConfig *skeleton,
   crtc = g_list_nth_data (combined_crtcs, crtc_id);
   g_list_free (combined_crtcs);
 
-  klass = META_MONITOR_MANAGER_GET_CLASS (manager);
-  if (klass->get_crtc_gamma)
-    klass->get_crtc_gamma (manager, crtc, &size, &red, &green, &blue);
-  else
-    {
-      size = 0;
-      red = green = blue = NULL;
-    }
+  meta_monitor_manager_get_crtc_gamma (manager, crtc,
+                                       &size, &red, &green, &blue);
 
   red_bytes = g_bytes_new_take (red, size * sizeof (unsigned short));
   green_bytes = g_bytes_new_take (green, size * sizeof (unsigned short));
@@ -3078,6 +3123,16 @@ meta_monitor_manager_is_transform_handled (MetaMonitorManager  *manager,
   return manager_class->is_transform_handled (manager, crtc, transform);
 }
 
+static void
+update_night_light_supported (MetaMonitorManager *manager)
+{
+  gboolean night_light_supported;
+
+  night_light_supported = is_night_light_supported (manager);
+  meta_dbus_display_config_set_night_light_supported (manager->display_config,
+                                                      night_light_supported);
+}
+
 static void
 meta_monitor_manager_real_read_current_state (MetaMonitorManager *manager)
 {
@@ -3098,6 +3153,7 @@ meta_monitor_manager_real_read_current_state (MetaMonitorManager *manager)
     }
 
   rebuild_monitors (manager);
+  update_night_light_supported (manager);
 }
 
 void
diff --git a/src/backends/native/meta-monitor-manager-native.c b/src/backends/native/meta-monitor-manager-native.c
index fd5e7784ff..37a50f1d6f 100644
--- a/src/backends/native/meta-monitor-manager-native.c
+++ b/src/backends/native/meta-monitor-manager-native.c
@@ -381,15 +381,30 @@ meta_monitor_manager_native_get_crtc_gamma (MetaMonitorManager  *manager,
   MetaKmsCrtc *kms_crtc;
   const MetaKmsCrtcState *crtc_state;
 
-  g_return_if_fail (META_IS_CRTC_KMS (crtc));
+  if (!META_IS_CRTC_KMS (crtc))
+    {
+      if (size)
+        *size = 0;
+      if (red)
+        *red = NULL;
+      if (green)
+        *green = NULL;
+      if (blue)
+        *blue = NULL;
+      return;
+    }
 
   kms_crtc = meta_crtc_kms_get_kms_crtc (META_CRTC_KMS (crtc));
   crtc_state = meta_kms_crtc_get_current_state (kms_crtc);
 
-  *size = crtc_state->gamma.size;
-  *red = g_memdup2 (crtc_state->gamma.red, *size * sizeof **red);
-  *green = g_memdup2 (crtc_state->gamma.green, *size * sizeof **green);
-  *blue = g_memdup2 (crtc_state->gamma.blue, *size * sizeof **blue);
+  if (size)
+    *size = crtc_state->gamma.size;
+  if (red)
+    *red = g_memdup2 (crtc_state->gamma.red, *size * sizeof **red);
+  if (green)
+    *green = g_memdup2 (crtc_state->gamma.green, *size * sizeof **green);
+  if (blue)
+    *blue = g_memdup2 (crtc_state->gamma.blue, *size * sizeof **blue);
 }
 
 static char *
diff --git a/src/backends/x11/meta-monitor-manager-xrandr.c b/src/backends/x11/meta-monitor-manager-xrandr.c
index 98eb080b6b..865f4e5800 100644
--- a/src/backends/x11/meta-monitor-manager-xrandr.c
+++ b/src/backends/x11/meta-monitor-manager-xrandr.c
@@ -707,9 +707,12 @@ meta_monitor_manager_xrandr_get_crtc_gamma (MetaMonitorManager  *manager,
                            (XID) meta_crtc_get_id (crtc));
 
   *size = gamma->size;
-  *red = g_memdup2 (gamma->red, sizeof (unsigned short) * gamma->size);
-  *green = g_memdup2 (gamma->green, sizeof (unsigned short) * gamma->size);
-  *blue = g_memdup2 (gamma->blue, sizeof (unsigned short) * gamma->size);
+  if (red)
+    *red = g_memdup2 (gamma->red, sizeof (unsigned short) * gamma->size);
+  if (green)
+    *green = g_memdup2 (gamma->green, sizeof (unsigned short) * gamma->size);
+  if (blue)
+    *blue = g_memdup2 (gamma->blue, sizeof (unsigned short) * gamma->size);
 
   XRRFreeGamma (gamma);
 }
diff --git a/src/org.gnome.Mutter.DisplayConfig.xml b/src/org.gnome.Mutter.DisplayConfig.xml
index c6859c2c09..5f85c5e271 100644
--- a/src/org.gnome.Mutter.DisplayConfig.xml
+++ b/src/org.gnome.Mutter.DisplayConfig.xml
@@ -297,6 +297,13 @@
     -->
     <property name="ApplyMonitorsConfigAllowed" type="b" access="read" />
 
+    <!--
+        NightLightSupported:
+
+        Whether night light is supported by this system.
+    -->
+    <property name="NightLightSupported" type="b" access="read" />
+
     <!--
         MonitorsChanged:
 
-- 
2.37.1

